#include<stdio.h>
int sum_of_digits(int num);
int main()
{
int num,sum;
printf("Enter the number");
scanf("%d",&num);
sum = sum_of_digits(num);
printf("The sum of digits of the number %d is %d",num,sum);
return 0;
}
int sum_of_digits(int num)
{
int Sum,temp;
while(num!=0)
{
temp=num%10;
Sum=Sum+temp;
num=num/10;
}
return Sum;
}