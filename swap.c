#include<stdio.h>
void swap(int *a,int *b);
int main()
{
int n1,n2;
printf("Enter two numbers");
scanf("%d%d",&n1,&n2);
printf("\nThe value of n1 and n2 before call or swapping: %d %d",n1,n2);
swap(&n1,&n2);
printf("\nThe value of n1 and n2 after call or swapping: %d %d\n",n1,n2);
return 0;
}
void swap(int *a,int *b)
{
 int temp;
 temp = *a;
 *a = *b;
 *b = temp;
 }